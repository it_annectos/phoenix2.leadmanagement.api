/**
 * ApplicationConfig.js
 *
 * @created          :: Ajeet
 * @Created  Date    :: 10/01/2017
 */

"use strict";

var request = require('request');

//code to allow self signed certificates
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};


module.exports = {


    /**
     *	Method		:: getApplicationConfigDetails
     *	Description :: used to retrieve Application Configurations
     *
     */
    getApplicationConfigDetails: function(setAppConfigCallback, passOnCallbak) {
        var appConfig;

        /*  // Retrieve Application Configuration
          ApplicationConfig.find().limit(1)
            .exec(function (err, result) {
              if(result.length){

                appConfig = result[0];
                setAppConfigCallback(appConfig, passOnCallbak);
              }else if(!err){

                appConfig = false;
                sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", result);
                setAppConfigCallback(appConfig, passOnCallbak);

              }else {

                appConfig = false; 
                sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", err);
                setAppConfigCallback(appConfig, passOnCallbak);

              }
            });*/

        var requestOptions = {
            url: 'http://testwinserver.annectos.net:1331/getApplicationConfig/v1',
            // url: 'http://phoenix2backend.goldencircle.in:1331/getApplicationConfig/v1',
            method: 'post',
            headers: {
                'authorization': 'Bearer eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4NDA1NjQ0OTk3NCIsImlhdCI6MTQ4NDE0Mzk4OH0.N5jQWjw2H7SP9fqTYSI-thBNRvG6DjcXRklDgch_7Dd4wra9lK_u2KB-5yqxDHeeKpQScxwCuHeuXKnbpmPjOw'
            },
            json: {}
        };
        // console.log(requestOptions);
        request(requestOptions, function(error, response, body) {
            //console.log("error in app config", error, "response in app config", response, "body in app config", body);
            if (!error) {
                if (body.statusCode === 0) {
                    appConfig = body.result[0];
                    setAppConfigCallback(appConfig, passOnCallbak);
                } else {
                    appConfig = false;
                    sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", body);
                    setAppConfigCallback(appConfig, passOnCallbak);
                }
            } else {
                appConfig = false;
                sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", body);
                setAppConfigCallback(appConfig, passOnCallbak);
            }
        })



    }
};